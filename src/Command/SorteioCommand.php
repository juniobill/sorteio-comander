<?php
namespace SorteioCommand\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SorteioCommand extends Command
{

    protected function configure()
    {
        $this->setName('sorteio')
            ->setDescription('Ferramenta para sorteios')
            ->setHelp('Este comando permite realizar sorteios')
            ->addArgument('participantes', InputArgument::IS_ARRAY | InputArgument::REQUIRED, 'Nome dos participantes')
            ->addOption('premio', '-p', InputOption::VALUE_OPTIONAL, 'Informe o premio', 'Desconhecido');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Sorteio');
        $io->section('Participantes');

        $participants = $input->getArgument('participantes');

        $io->listing($participants);

        $hasNewParticipant = false;
        if ($input->isInteractive()) {
            do {
                $addNew = $io->ask('Deseja adicionar novo participante (s/N)?', 'N', function ($answer) {
                    $answer = strtolower($answer);
                    if ($answer != 's' && $answer != 'n') {
                        throw new \RuntimeException('A resposta deve ser sim (s) ou não (n)!');
                    }

                    return $answer;
                });

                if ($addNew == 's') {
                    $participants[] = $io->ask('Informe o nome do novo participante');
                    $hasNewParticipant = true;
                }
            } while ($addNew == 's');
        }

        if ($hasNewParticipant) {
            $io->comment('Novos participantes adicionados');
            $io->section('Participantes');
            $io->listing($participants);
        }

        $io->block($input->getOption('premio'), 'PREMIO', 'fg=white;bg=blue', ' ', true);

        $winner = array_rand($participants);

        $io->block($participants[$winner], 'VENCEDOR', 'fg=black;bg=green', ' ', true);
    }
}
